# yui
a turn-based RPG for the Nintendo 3DS starring a cute fox on an adventure to stop the darkness
<br>Made in Godot[^1]
### project goals
hopefully a completed game? as of right now, this is only meant to be a prototype. goals for this prototype are:

- fully featured engine with dialogue, asset management, entity system and interaction, scene management, and midi audio playback
- 2 test maps, and an early version of the first map
- fully featured gameplay and battle system

### Dependencies
Build instructions:
it's a godot project, so building is handled by the engine. just go to Project->Export. build templates *should* be already set up for windows and linux, however, I don't have a macOS device, so I cannot test executables built for macOS. testing on macOS would be appreciated!

### Project goals(for the future)
I haven't fully decided on the final platform that this game will be on. what I *do* know is that I'm planning on having
2 "releases". a port to some older nintendo hardware(likely either the DS or 3DS, since I have both hardware and ways to test games on them)
and a PC release that'll be on itch.io and *possibly* steam. 

### Contribution
If you want to contribute, go ahead!! there's a public obsidian vault for the project in `.obsidian/` in the root of the repo, which has Todo lists, dialogue, plans, etc. this is updated as I work on the project, so you should just be able to jump right in, check the TODO lists and see if something needs done that catches your eye. the TODO lists are in a format where categories are markdown files in all caps, and the individual TODO lists are in the folders based on the categories. 


[^1]: I was orginally planning on writing this in crablang/rust, but I've decided against that now due to the fact that I know absolutely nothing
about the language yet, and didn't want to fight with concepts I didn't know yet. *then* I was planning on
writing this in C with SDL2, then C with Citro2D. I have now decided against those, cause I just don't have the
knowledge required for that. so I'm doing it in Godot now. hopefully this will be the final switch
