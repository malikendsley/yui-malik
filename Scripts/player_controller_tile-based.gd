extends CharacterBody2D

# setup basic player types and enums (exported variables at the top, and non-exported ones below those)
enum PLAYER_STATE {
	IDLE,
	WALK,
	INTERACT
};

enum PLAYER_DIRECTION {
	UP,
	DOWN,
	LEFT,
	RIGHT
};

@onready var raycast = $RayCast2D
@onready var sprite_anim = $AnimatedSprite2D
@export var lerp_speed = 1
@export var state = PLAYER_STATE.IDLE # default to the idle state
@export var direction = PLAYER_DIRECTION.UP # default direction is up

# unused for now, but the idea is that this will be the amount of times you can
# use the elemental stone's power in battles. kind of like the star meter in 
# Paper Mario: The Thousand Year Door
var stone_power = 0;
var level = 1;
var health = 100;
var experience = 0.0;
var tile_size = 16
var is_lerping = false # whether the player is visually moving

var collider = null

# using a 2D vector to represent input implicitly defines the zero vector as no input
var input_vector = Vector2.ZERO

func _ready():
	position = position.snapped(Vector2.ONE * tile_size)
	position += Vector2.ONE * tile_size / 2

func handle_states():
	if Input.is_action_pressed("move_up"):
		direction = PLAYER_DIRECTION.UP
	elif Input.is_action_pressed("move_down"):
		direction = PLAYER_DIRECTION.DOWN
	elif Input.is_action_pressed("move_left"):
		direction = PLAYER_DIRECTION.LEFT
	elif Input.is_action_pressed("move_right"):
		direction = PLAYER_DIRECTION.RIGHT

	match state:
		PLAYER_STATE.IDLE:
			on_idle()
		PLAYER_STATE.WALK:
			on_walk()
		PLAYER_STATE.INTERACT:
			on_interact()

func on_idle():
	match direction:
		PLAYER_DIRECTION.UP:
			sprite_anim.play("idle_up")
		PLAYER_DIRECTION.DOWN:
			sprite_anim.play("idle_down")
		PLAYER_DIRECTION.LEFT:
			sprite_anim.play("idle_left")
		PLAYER_DIRECTION.RIGHT:
			sprite_anim.play("idle_right")
		
	# if the player is inputting, change the state to walking
	if input_vector != Vector2.ZERO:
		state = PLAYER_STATE.WALK

func on_walk():
	# prevent re-entry
	if is_lerping:
		return

	# if controls are released, go back to idle
	if input_vector == Vector2.ZERO:
		state = PLAYER_STATE.IDLE
	# otherwise, try to move the player
	else:
		try_move_player(input_vector)

	# check for interactions (this may not work given i never edited them to play nice with changes)
	if raycast.is_colliding():
		if raycast.get_collider().is_in_group("Interactable"):
			state = PLAYER_STATE.INTERACT

func on_interact():
	if raycast.is_colliding():
		sprite_anim.stop()
		if raycast.get_collider().is_in_group("Interactable") and Input.is_action_just_pressed("Interact"):
			print("Squeak!") # print test message for now. this will be swapped out for whatever NPC I have's dialogue.
	
	elif input_vector != Vector2.ZERO:
		state = PLAYER_STATE.WALK
	else:
		state = PLAYER_STATE.IDLE

func try_move_player(move_dir: Vector2):
	# you may want to migrate all directions to use vectors, i find it easier to work with
	match move_dir:
		Vector2.UP:
			sprite_anim.play("walk_up")
		Vector2.DOWN:
			sprite_anim.play("walk_down")
		Vector2.LEFT:
			sprite_anim.play("walk_left")
		Vector2.RIGHT:
			sprite_anim.play("walk_right")
	# adjust raycast position to 1 tile ahead of facing direction
	raycast.target_position = global_position + move_dir * tile_size
	raycast.force_raycast_update()

	# collision check
	if !raycast.is_colliding():
		is_lerping = true
		var tween = create_tween()
		tween.tween_property(self, "position", position + move_dir * tile_size, 0.5 / lerp_speed)
		await get_tree().create_timer(0.5 / lerp_speed).timeout
		is_lerping = false
	else:
		# we didn't actually move because we're blocked
		print("Movement blocked")
		state = PLAYER_STATE.IDLE

func _process(_delta):
	# gather input
	input_vector.x = int(Input.is_action_pressed("move_right")) - int(Input.is_action_pressed("move_left"))
	input_vector.y = int(Input.is_action_pressed("move_down")) - int(Input.is_action_pressed("move_up"))
	
	# if x is not 0, y is 0, and vice versa
	if input_vector.x != 0:
		input_vector.y = 0
	elif input_vector.y != 0:
		input_vector.x = 0

	# test object interaction
	handle_states()