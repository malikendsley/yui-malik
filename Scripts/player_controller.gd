extends CharacterBody2D

# setup basic player types and enums (exported variables at the top, and non-exported ones below those)
enum PLAYER_STATE {
	IDLE,
	WALK,
	INTERACT
};

enum PLAYER_DIRECTION {
	UP,
	DOWN,
	LEFT,
	RIGHT
};

@onready var sprite_anim = $AnimatedSprite2D;
@export var state = PLAYER_STATE.IDLE;       # default to the idle state
@export var direction = PLAYER_DIRECTION.UP; # default direction is up
@export var player_speed = 3000;

# unused for now, but the idea is that this will be the amount of times you can
# use the elemental stone's power in battles. kind of like the star meter in 
# Paper Mario: The Thousand Year Door
var stone_power = 0;
var level = 1;
var health = 100;
var experience = 0.0;
var directionalInput = Vector2.ZERO;


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Handles the state of the player, including interactions, playing animations
# and starting battles. still unsure whether or not battles will be turn based
# or action based, or some other unique battle system
func handle_states(delta):
	# set the direction each frame based on dpad buttons pressed
	if Input.is_action_pressed("move_up"):
		direction = PLAYER_DIRECTION.UP;
	elif Input.is_action_pressed("move_down"):
		direction = PLAYER_DIRECTION.DOWN;
	elif Input.is_action_pressed("move_left"):
		direction = PLAYER_DIRECTION.LEFT;
	elif Input.is_action_pressed("move_right"):
		direction = PLAYER_DIRECTION.RIGHT;
	
	match state:
		PLAYER_STATE.IDLE:
			on_idle();
		PLAYER_STATE.WALK:
			on_walk();
		PLAYER_STATE.INTERACT:
			on_interact();
		_:
			pass;
func on_idle():
	match direction:
		PLAYER_DIRECTION.UP:
			sprite_anim.play("idle_up");
		PLAYER_DIRECTION.DOWN:
			sprite_anim.play("idle_down");
		PLAYER_DIRECTION.LEFT:
			sprite_anim.play("idle_left");
		PLAYER_DIRECTION.RIGHT:
			sprite_anim.play("idle_right");
		_:
			sprite_anim.play("idle_down");
		
	if Input.is_action_pressed("move_left") or Input.is_action_pressed("move_right") or Input.is_action_pressed("move_up") or Input.is_action_pressed("move_down"):
		state = PLAYER_STATE.WALK;

func on_walk():
	match direction:
		PLAYER_DIRECTION.UP:
			sprite_anim.play("walk_up");
		PLAYER_DIRECTION.DOWN:
			sprite_anim.play("walk_down");
		PLAYER_DIRECTION.LEFT:
			sprite_anim.play("walk_left");
		PLAYER_DIRECTION.RIGHT:
			sprite_anim.play("walk_right");
			
	if not (Input.is_action_pressed("move_left") or Input.is_action_pressed("move_right") or Input.is_action_pressed("move_up") or Input.is_action_pressed("move_down")):
		state = PLAYER_STATE.IDLE;
	
func on_interact():
	pass;

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	directionalInput = Input.get_vector("move_left", "move_right", "move_up", "move_down");
	velocity = directionalInput * player_speed * delta
	
	# prevent diagonal movement by setting the opposite velocity to 0 if an axis is pressed. 
	# might remove later if I find a better way to do this, as this is a little hacky. 
	if Input.is_action_pressed("move_down") or Input.is_action_pressed("move_up"):
		velocity.x = 0;
	elif Input.is_action_pressed("move_left") or Input.is_action_pressed("move_right"):
		velocity.y = 0;
	else:
		velocity = Vector2.ZERO;
	
	handle_states(delta)
	move_and_slide()
